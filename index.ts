import express from 'express';
import * as fs from 'fs';

const app = express();
const PORT = 3000;
const bodyParser = require('body-parser');

type IUser = {
    name:string,
    password:string,
    id:string,
    books:string[],
    books_history:string[]
}
type BookStatus = {
    id: string,
    status: string,
    borrower_id: string,
    due_date: string
}
type IBook = {
    isbn: string,
    title: string,
    author: string,
    published: string,
    publisher: string,
    pages: number,
    description:string,
    copies: BookStatus []
}
const users: IUser[] = [{
    name: 'Anup Poudel',
    password: 'password',
    id: '12345678',
    books: ['Ulysses'],
    books_history:['23.10.2020']
}];

const loadBooks = (fileName = './books.json') => {
    return JSON.parse(fs.readFileSync(fileName, 'utf8'));
};

const libraryBooks: IBook[] = loadBooks();


app.use(bodyParser.json());
//app.use(express.json());
// app.use(express.urlencoded({extended:false})); // handle url encoded data

app.get('/', (req,res) => {
    res.send('My library!');
})

//H5.1 Create a new user with POST
app.post('/library/new_user',(req,res) => {
    const myUser: {name:string, password:string} = req.body;
    const foundUser = users.find(e => (e.name === myUser.name) && (e.password === myUser.password));
    if (!foundUser){
    const user:IUser = {
        name: myUser.name,
        password: myUser.password,
        id: Date.now().toString(),
        books: [],
        books_history:[]
    }   
    console.log(user);
    users.push(user);
    res.json(user);
    }else if (foundUser) {
        res.send ('User already registered.');
    }
})

//H5.2 GET number of borrowed books
    app.get('/library/user_id/books_n', (req,res) => {
    const user_id:{id:string}  = req.body;
    const regUser = users.find(e => e.id === user_id.id);
    if (regUser){
        res.send (regUser.books.length.toString());
    } else {
        res.send (`No user found with id = ${user_id.id}`);
    }

    })

    
 
//   H5.3 GET a list of borrowed books
    app.get ('/library/user_id/books', (req,res) => {
    const user_id:{id:string} = req.body;
    const regUser = users.find(e => e.id === user_id.id);
    if (regUser){
        res.json(regUser.books);
    }else {
        res.send(`No user found with id = ${user_id.id}`);
    }
    })
    

// H5.4 Borrow a book with PATCH

app.patch('/library/user/borrow', (req,res) => {
    const userInput:{id:string, password:string, isbn:string} = req.body;
    const regUser:IUser|undefined = users.find(e => e.id === userInput.id && e.password === userInput.password);
    if (regUser){

        const borrowBook = libraryBooks.find(book => book.isbn === userInput.isbn);
        if (borrowBook){
            const available = borrowBook.copies.find(book => book.status === "in_library");
            if (available){
                regUser.books.push(borrowBook.title);  
                regUser.books_history.push((new Date().setDate(new Date().getDate() + 30)).toString());  
                res.json(regUser);      
            }else if (!available){
                res.send(`Book not available for borrowing.`);
            }
        }else if (!borrowBook){
            res.send(`No book found with isbn = ${userInput.isbn}`);
        }
    }else {
        res.send(`Incorrect user details`);
    }
})

app.patch('/library/user/return', (req,res) => {
    const userInput:{id:string, password:string, isbn:string} = req.body;
    const regUser:IUser|undefined = users.find(e => e.id === userInput.id && e.password === userInput.password);
    if (regUser){
        const returnBook = libraryBooks.find(book => book.isbn === userInput.isbn);
        if (returnBook){
            console.log(regUser);
            res.json(returnBook);
        }else if (!returnBook){
            res.send(`Please enter correct isbn.`);
        }
    }else{
        res.send(`Invalid user details`);
    }
})


app.listen(PORT, () => {
    console.log (`Running the server in ${PORT}`);
})